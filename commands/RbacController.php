<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 14:16
 */

namespace app\commands;


use app\components\helpers\UserHelper;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;

        $perm_mng_notices = $authManager->createPermission(UserHelper::PERM_MANAGE_NOTICES);
        $perm_mng_notices->description = 'CRUD for Notifications';
        $authManager->add($perm_mng_notices);

        $perm_mng_posts = $authManager->createPermission(UserHelper::PERM_MANAGE_POSTS);
        $perm_mng_posts->description = 'CRUD for Posts';
        $authManager->add($perm_mng_posts);

        $perm_mng_users = $authManager->createPermission(UserHelper::PERM_MANAGE_USERS);
        $perm_mng_users->description = 'CRUD for Users';
        $authManager->add($perm_mng_users);

        $perm_mng_notice_logs = $authManager->createPermission(UserHelper::PERM_MANAGE_NOTICE_LOGS);
        $perm_mng_notice_logs->description = 'View/Close notifications logs';
        $authManager->add($perm_mng_notice_logs);

        $perm_view_post = $authManager->createPermission(UserHelper::PERM_VIEW_POST);
        $perm_view_post->description = 'View Post';
        $authManager->add($perm_view_post);

        $role_admin = $authManager->createRole(UserHelper::ROLE_ADMIN);
        $role_admin->description = 'admin role';

        $role_user = $authManager->createRole(UserHelper::ROLE_USER);
        $role_user->description = 'auth user role';

        $authManager->add($role_user);
        $authManager->add($role_admin);

        $authManager->addChild($role_user, $perm_mng_notice_logs);
        $authManager->addChild($role_user, $perm_view_post);

        $authManager->addChild($role_admin, $perm_mng_posts);
        $authManager->addChild($role_admin, $perm_mng_notices);
        $authManager->addChild($role_admin, $perm_mng_users);

        $authManager->addChild($role_admin, $role_user);
    }
} 