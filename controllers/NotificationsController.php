<?php

namespace app\controllers;

use app\components\helpers\DebugHelper;
use app\components\helpers\FlashHelper;
use app\components\helpers\UserHelper;
use app\models\NotificationsLogs;
use Codeception\Module\DumbHelper;
use Yii;
use app\models\Notifications;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotificationsController implements the CRUD actions for Notifications model.
 */
class NotificationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'close-log' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => [UserHelper::ROLE_ADMIN],
                    ],
                    [
                        'actions' => ['close-log', 'close-all', 'logs'],
                        'allow'   => true,
                        'roles'   => [UserHelper::ROLE_USER],
                    ],
                ],

            ],
        ];
    }

    /**
     * Lists all Notifications models.
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Notifications::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Notifications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notifications();

        if(Yii::$app->request->isPost) {
            DebugHelper::flush(print_r(Yii::$app->request->post(), true));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notifications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->type = $model->getType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionLogs() {
        $logs = NotificationsLogs::getUserNotifications(UserHelper::getId(), Notifications::TYPE_BROWSER);

        $provider = new ArrayDataProvider([
            'allModels' => $logs,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('logs', [
            'dataProvider' => $provider,
        ]);
    }

    public function actionCloseLog() {
        $id = Yii::$app->request->post('id');

        $response = [ 'success' => false ];

        if(!empty($id)) {
            $response['success'] = NotificationsLogs::closeLog($id);
        }

        echo Json::encode($response);
    }

    public function actionCloseAll() {
        NotificationsLogs::closeLogsByUser(UserHelper::getId());

        FlashHelper::setFlashSuccess('All notifications successfully closed');

        return $this->redirect(['logs']);
    }

    /**
     * Finds the Notifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Notifications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notifications::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
