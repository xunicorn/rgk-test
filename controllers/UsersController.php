<?php

namespace app\controllers;

use app\components\helpers\FlashHelper;
use app\components\helpers\UserHelper;
use Yii;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'changeActive', 'changeRole'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserHelper::ROLE_ADMIN],
                    ]
                ],

            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Users::createUser($model->username, $model->password, $model->email, $model->roles);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $active_state = $model->active;

        $prev_password = $model->password;

        $model->roles = Users::getUserRoles($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($prev_password != $model->password) {
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            }

            $model->save();

            if($model->active != $active_state) {
                Users::changeActiveState($id, $model->active);
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangeActive($id) {
        $this->findModel($id);

        $active = \Yii::$app->request->post('active');

        $success = Users::changeActiveState($id, $active);

        if(!Yii::$app->request->isAjax) {
            if($success) {
                FlashHelper::setFlashSuccess('User active state successfully changed');
            } else {
                FlashHelper::setFlashError('Occurred some problems in changing user active state');
            }

            $this->redirect(Yii::$app->request->getReferrer());
        } else {
            $response = [ 'success' => $success, 'active' => $active ];
            echo Json::encode($response);
        }
    }

    public function actionChangeRole($id) {
        $this->findModel($id);

        $role = Yii::$app->request->post('role');

        Yii::$app->authManager->revokeAll($id);


        $role = Yii::$app->authManager->getRole($role);
        Yii::$app->authManager->assign($role, $id);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
