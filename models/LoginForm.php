<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    public $password_verified;
    public $email;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],

            [['username', 'password', 'password_verified', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            ['password', 'string', 'min' => 5],

            // password is validated by validatePassword()
            ['password', 'validatePassword', 'on' => 'login'],

            [['password', 'password_verified'], 'verifyPasswords', 'on' => 'register'],
            ['email', 'unique', 'targetClass' => '\app\models\Users','on' => 'register'],
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function verifyPasswords($attribute, $params)
    {
        if(!$this->hasErrors()) {
            if(empty($this->$attribute)) {
                $this->addError($attribute, 'Password could not to be empty');
            } elseif($this->password != $this->password_verified) {
                $this->addError($attribute, 'Password & password verified - mismatch');
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    public function register() {
        if($this->validate()) {
            return Users::createUser($this->username, $this->password, $this->email);
            //return true;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Users|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->username);
        }

        return $this->_user;
    }
}
