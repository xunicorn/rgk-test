<?php

namespace app\models;

use app\components\events\PostCreateEvent;
use app\components\helpers\UserHelper;
use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $text_short
 * @property string $text
 * @property string $date
 * @property Users $user
 */
class Posts extends \yii\db\ActiveRecord
{
    const EVENT_POST_CREATE = 'post_create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'date'], 'required'],
            [['user_id', 'date'], 'integer'],
            [['text', 'text_short'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'title' => 'Title',
            'text' => 'Text',
            'text_short' => 'Text Short',
            'date' => 'Date',
        ];
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function createPost($title, $text, $text_short, $user_id = false) {
        if(empty($text_short)) {
            $text_short = substr($text, 0, 255);
        }

        $post             = new Posts();
        $post->title      = $title;
        $post->text       = $text;
        $post->text_short = $text_short;
        $post->date       = time();

        $post->user_id    = empty($user_id) ? UserHelper::getId() : $user_id;

        if($post->save()) {
            $event = new PostCreateEvent();
            $event->id    = $post->id;
            $event->title = $title;
            $event->text_short = $text_short;

            $post->trigger(Posts::EVENT_POST_CREATE, $event);

            return $post->id;
        }

        return false;
    }

}
