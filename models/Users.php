<?php

namespace app\models;

use app\components\events\UserActiveEvent;
use app\components\events\UserRegistrationEvent;
use app\components\helpers\UserHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $active
 * @property integer $date_reg
 * @property string $auth_key
 * @property string $access_token
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{

    const EVENT_USER_REG    = 'user_registration';
    const EVENT_USER_ACTIVE = 'user_active';

    public $roles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username', 'password', 'date_reg'], 'required'],
            [['active', 'date_reg'], 'integer'],
            [['email', 'username', 'password'], 'string', 'max' => 255],
            [['auth_key', 'access_token'], 'string', 'max' => 128],
            ['password', 'string', 'min' => 5],
            ['email', 'email'],
            [['email', 'username'], 'unique'],
            ['roles', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'email'    => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'active'   => 'Active',
            'date_reg' => 'Reg Date',
            'roles'    => 'Roles',
        ];
    }

    //region Identity
    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    //endregion

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key     = \Yii::$app->security->generateRandomString();
                $this->access_token = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(!empty($this->roles)) {
            if(!is_array($this->roles)) {
                $this->roles = (array)$this->roles;
            }

            Yii::$app->authManager->revokeAll($this->id);

            foreach($this->roles as $_role) {
                $userRole = Yii::$app->authManager->getRole($_role);
                Yii::$app->authManager->assign($userRole, $this->id);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public static function getUserRoles($id) {
        $roles = \Yii::$app->authManager->getRolesByUser($id);

        //echo '<pre>'; print_r($roles); echo '</pre>';

        return ArrayHelper::map($roles, 'name', 'name');
    }

    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @param $username
     * @return Users|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    public static function getUsers() {
        return Users::find()->all();
    }

    public static function createUser($username, $password, $email, $roles = []) {
        $user = new Users();
        $user->email = $email;
        $user->username = $username;
        $user->password = Yii::$app->getSecurity()->generatePasswordHash($password);
        $user->date_reg = time();

        $ar_count = static::find()->count();


        $user->roles = empty($roles) ? [UserHelper::ROLE_USER] : $roles;

        if($ar_count == 0) {
            $user->roles = UserHelper::ROLE_ADMIN;
        }


        if($user->save()) {
            $event = new UserRegistrationEvent;
            $event->username = $username;
            $event->password = $password;
            $event->email    = $email;

            $user->trigger(self::EVENT_USER_REG, $event);

            return true;
        }

        return false;
    }

    public static function changeActiveState($user_id, $active) {
        $user = Users::findOne(['id' => $user_id]);

        if(empty($user)) {
            return false;
        }

        $user->active = intval($active);

        if($user->save()) {
            $event = new UserActiveEvent;

            $event->active   = $user->active;
            $event->username = $user->username;
            $event->email    = $user->email;

            $user->trigger(self::EVENT_USER_ACTIVE, $event);

            return true;
        }

        return false;
    }
}
