<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "notifications_logs".
 *
 * @property string $id
 * @property string $notification_id
 * @property string $date
 * @property string $user_id
 * @property string $text
 * @property string $type
 * @property integer $closed
 * @property Notifications $notification
 */
class NotificationsLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'date', 'user_id', 'text', 'type'], 'required'],
            [['notification_id', 'date', 'user_id', 'closed'], 'integer'],
            ['text', 'safe'],
            ['type', 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_id' => 'Notification ID',
            'date' => 'Date',
            'user_id' => 'User',
            'text' => 'Text',
            'type' => 'Type',
        ];
    }

    public static function createLog($notification_id, $user_id, $body, $type) {
        $log = new NotificationsLogs();

        $log->notification_id = $notification_id;
        $log->user_id         = $user_id;
        $log->date            = time();
        $log->text            = $body;
        $log->type            = $type;

        return $log->save();
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['user_id' => 'id']);
    }

    public function getNotification() {
        return $this->hasOne(Notifications::className(), ['id' => 'notification_id']);
    }

    /**
     * @param $user_id
     * @param $type
     * @return NotificationsLogs[]
     */
    public static function getUserNotifications($user_id, $type) {
        return static::find()
            ->where(['user_id' => $user_id, 'type' => $type])
            ->orderBy('closed, id DESC')
            ->all();
    }

    public static function closeLog($id) {
        return static::updateAll(['closed' => 1], ['id' => $id]);
    }

    public static function closeLogsByUser($user_id) {
        return static::updateAll(['closed' => 1], ['user_id' => $user_id]);
    }

}
