Dear {username}. On our {siteName} was posted new article.<br/>

<h4>{articleName}</h4>
<p>{articleShort}</p>
<p>{readMoreLink}</p>
<br/>
Best regards {siteName} Team.