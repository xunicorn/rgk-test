<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $title string */
/* @var $serviceName string */
/* @var $serviceLink string */
/* @var $active string */
/* @var $username string */

$action = (empty($active) ? 'disabled' : 'activated');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= $title; ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    Your account on <?= Html::a($serviceName, $serviceLink)?> was <?= $action?>. <br/>
    <br/>
    Best regards <?= $serviceName ?> Team.
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>