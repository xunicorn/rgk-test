<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $title string */
/* @var $serviceName string */
/* @var $serviceLink string */
/* @var $password string */
/* @var $login string */
?>

    Greeting on our <?= $serviceName ?>!<br/>
    Your email was registered on <?= Html::a($serviceName, $serviceLink); ?>.<br/>
    -------------------------<br/>
    login: <?= $login; ?><br/>
    password: <?= $password; ?><br/>
    -------------------------<br/>
    <br/>
    Best regards <?= $serviceName ?> Team.
