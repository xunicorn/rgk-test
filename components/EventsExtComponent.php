<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.04.2016
 * Time: 18:34
 */

namespace app\components;


use app\components\events\PostCreateEvent;
use app\components\events\UserActiveEvent;
use app\components\events\UserRegistrationEvent;
use app\components\helpers\MailerHelper;
use app\components\helpers\ModelEventsHelper;
use app\models\Notifications;
use app\models\NotificationsLogs;
use app\models\Posts;
use app\models\Users;
use yii\base\Component;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class EventsExtComponent extends Component {
    public function init()
    {
        parent::init();

        Event::on(Users::className(), Users::EVENT_USER_REG, [$this, 'onUserReg']);

        Event::on(Users::className(), Users::EVENT_USER_ACTIVE, [$this, 'onUserActive']);

        Event::on(Posts::className(), Posts::EVENT_POST_CREATE, [$this, 'onPostCreate']);
    }


    public function onUserReg(UserRegistrationEvent $event) {
        $this->sendNotifications($event, Users::EVENT_USER_REG);

        if(\Yii::$app->params['useDefaultNotice'])
            MailerHelper::sendRegistrationMail($event->email, $event->username, $event->password);
    }

    public function onUserActive(UserActiveEvent $event) {

        $this->sendNotifications($event, Users::EVENT_USER_ACTIVE);


        if(\Yii::$app->params['useDefaultNotice'])
            MailerHelper::sendUserActiveMail($event->email, $event->username, $event->active);
    }

    public function onPostCreate(PostCreateEvent $event) {

        $this->sendNotifications($event, Posts::EVENT_POST_CREATE);


        if(\Yii::$app->params['useDefaultNotice']) {
            $users = Users::getUsers();

            $emails    = ArrayHelper::map($users, 'id', 'email');
            $usernames = ArrayHelper::map($users, 'id', 'username');

            MailerHelper::sendPostCreateMail($emails, $usernames, $event->id, $event->title, $event->text_short);

            $browser_template_path = \Yii::getAlias('@app') . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, ['data', 'events-default', 'browser', 'post_created_browser.php']);

            $browser_template = file_get_contents($browser_template_path);

            foreach($usernames as $_user_id => $_username) {
                $replace = [
                    Notifications::FIELD_SITE_NAME             => Html::a(\Yii::$app->name, Url::base('http')),
                    Notifications::FIELD_USERNAME              => $_username,
                    Notifications::FIELD_ARTICLE_NAME          => $event->title,
                    Notifications::FIELD_ARTICLE_SHORT         => $event->text_short,
                    Notifications::FIELD_ARTICLE_READMORE_LINK => Html::a('Read more', Url::to(['/posts/view', 'id' => $event->id], 'http')),
                ];

                $body = str_replace(array_keys($replace), $replace, $browser_template);

                NotificationsLogs::createLog(0, $_user_id, $body, Notifications::TYPE_BROWSER);
            }
        }
    }

    private function sendNotifications($event, $event_code) {
        $notifications = Notifications::getNotificationsByEvent($event_code);

        if(empty($notifications)) {
            return;
        }

        $users = Users::getUsers();

        $emails    = ArrayHelper::map($users, 'id', 'email');
        $usernames = ArrayHelper::map($users, 'id', 'username');

        $shortTags = $this->parseTags($event, $event_code);

        $messages = [];

        foreach($notifications as $_notif) {
            $types = $_notif->getType();

            $to   = $_notif->userTo->email;
            $from = $_notif->userFrom->email;

            $subject = $_notif->title;

            if($_notif->send_to_all) {
                foreach($usernames as $user_id => $_usrnm) {
                    $to = $emails[$user_id];

                    $shortTags[Notifications::FIELD_USERNAME] = $_usrnm;

                    $body = str_replace(
                        array_keys($shortTags),
                        $shortTags,
                        $_notif->text
                    );

                    if($_notif->isEmail()) {
                        $messages[] = MailerHelper::createCustomMessage($from, $to, $subject, $body);
                    }

                    foreach($types as $_type) {
                        NotificationsLogs::createLog($_notif->id, $user_id, $body, $_type);
                    }
                }
            } else {
                if($event instanceof UserRegistrationEvent) {
                    $shortTags[Notifications::FIELD_USERNAME] = $event->username;
                } else {
                    $shortTags[Notifications::FIELD_USERNAME] = $_notif->userTo->username;
                }

                $body = str_replace(
                    array_keys($shortTags),
                    $shortTags,
                    $_notif->text
                );

                if($_notif->isEmail()) {
                    $messages[] = MailerHelper::createCustomMessage($from, $to, $subject, $body);
                }

                foreach($types as $_type) {
                    NotificationsLogs::createLog($_notif->id, $_notif->user_id_to, $body, $_type);
                }
            }
        }

        MailerHelper::sendMultiple($messages);
    }

    private function parseTags($event, $eventCode) {
        $shortTags  = ModelEventsHelper::getCommonShortTags();

        $tags = ModelEventsHelper::getInfo($eventCode);

        foreach($tags as $_tag) {
            switch($_tag) {
                case Notifications::FIELD_USERNAME:
                    $shortTags[Notifications::FIELD_USERNAME] = ($event instanceof UserRegistrationEvent || $event instanceof UserActiveEvent)
                        ? $event->username : Notifications::FIELD_USERNAME;
                    break;

                case Notifications::FIELD_ARTICLE_NAME:
                    $shortTags[Notifications::FIELD_ARTICLE_NAME] = ($event instanceof PostCreateEvent) ? $event->title : Notifications::FIELD_ARTICLE_NAME;
                    break;

                case Notifications::FIELD_ARTICLE_SHORT:
                    $shortTags[Notifications::FIELD_ARTICLE_SHORT] = ($event instanceof PostCreateEvent) ? $event->text_short : Notifications::FIELD_ARTICLE_SHORT;
                    break;

                case Notifications::FIELD_ARTICLE_READMORE_LINK:
                    $shortTags[Notifications::FIELD_ARTICLE_READMORE_LINK] = ($event instanceof PostCreateEvent)
                        ? Html::a('Read more', Url::to(['/posts/view', 'id' => $event->id], 'http'))
                        : Notifications::FIELD_ARTICLE_READMORE_LINK;
                    break;

                case Notifications::FIELD_USER_PASS:
                    $shortTags[Notifications::FIELD_USER_PASS] = ($event instanceof UserRegistrationEvent) ? $event->password : Notifications::FIELD_USER_PASS;
                    break;

                case Notifications::FIELD_USER_STATE:
                    $shortTags[Notifications::FIELD_USER_STATE] = ($event instanceof UserActiveEvent) ? $event->active : Notifications::FIELD_USER_STATE;
                    break;

                default:
                    if(!isset($shortTags[$_tag])) {
                        $shortTags[$_tag] = $_tag;
                    }
                    break;

            }
        }

        return $shortTags;
    }
} 