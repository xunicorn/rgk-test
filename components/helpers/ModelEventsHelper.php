<?php
namespace app\components\helpers;

use app\models\Notifications;
use app\models\Posts;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

class ModelEventsHelper {

    const INFO_SHORT_TAGS = 'notificationFields';

    protected static $eventsRoadMap = [
        Posts::EVENT_POST_CREATE => [
            Notifications::FIELD_USERNAME,
            Notifications::FIELD_SITE_NAME,
            Notifications::FIELD_ARTICLE_NAME,
            Notifications::FIELD_ARTICLE_SHORT,
            Notifications::FIELD_ARTICLE_READMORE_LINK,
        ],
        Users::EVENT_USER_REG => [
            Notifications::FIELD_SITE_NAME,
            Notifications::FIELD_USERNAME,
            Notifications::FIELD_USER_PASS,
        ],
        Users::EVENT_USER_ACTIVE => [
            Notifications::FIELD_SITE_NAME,
            Notifications::FIELD_USERNAME,
            Notifications::FIELD_USER_STATE,
        ],
    ];

    /**
     * @param $eventName string|boolean its from Users::EVENT_*
     * @return string[]
     */
    public static function getInfo($eventName = false) {
        if(!empty($eventName)) {
            return isset(self::$eventsRoadMap[$eventName]) ? self::$eventsRoadMap[$eventName] : [];
        }

        return self::$eventsRoadMap;
    }

    /**
     * @return string[]
     */
    public static function getEvents() {
        $events = array_keys(self::$eventsRoadMap);

        return array_combine($events, $events);
    }

    public static function getCommonShortTags() {
        return [
            Notifications::FIELD_SITE_NAME => Html::a(\Yii::$app->name, Url::base('http')),
        ];
    }
}