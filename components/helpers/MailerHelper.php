<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 12:17
 */

namespace app\components\helpers;


use yii\helpers\Url;

class MailerHelper {

    public static function sendCustomMail($from, $to, $subject, $body) {
        \Yii::$app->mailer
            ->compose()
            ->setTextBody($body)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    public static function sendRegistrationMail($to, $username, $password) {
        $subject = 'Welcome to ' . \Yii::$app->name;
        $from    = \Yii::$app->params['adminEmail'];

        $params = [
            'title'       => $subject,
            'serviceName' => \Yii::$app->name,
            'serviceLink' => Url::home(true),
            'login'       => $username,
            'password'    => $password,
        ];

        \Yii::$app->mailer
            ->compose('@app/data/events-default/mail/registration', $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    public static function sendUserActiveMail($to, $username, $active) {
        $subject = 'Your account was ' . (empty($active) ? 'disabled' : 'enabled');
        $from    = \Yii::$app->params['adminEmail'];

        $params = [
            'title'       => $subject,
            'serviceName' => \Yii::$app->name,
            'serviceLink' => Url::home(true),
            'active'      => $active,
            'username'    => $username,
        ];

        \Yii::$app->mailer
            ->compose('@app/data/events-default/mail/user_active', $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    public static function sendPostCreateMail($to, $username, $articleId, $articleName, $articleShort) {
        $subject = 'New post was posted on ' . \Yii::$app->name;
        $from    = \Yii::$app->params['adminEmail'];

        $params = [
            'title'       => $subject,
            'serviceName' => \Yii::$app->name,
            'serviceLink' => Url::home(true),
            'articleId'   => $articleId,
            'articleName' => $articleName,
            'articleShort' => $articleShort,
            'username'     => $username,
        ];

        if(!is_array($to)) {
            $to = (array)$to;
            $username = (array)$username;
        }

        $messages = array();

        foreach($to as $i => $_to) {
            $_username = $username[$i];

            $params['username'] = $_username;

            $messages[] = \Yii::$app->mailer
                ->compose('@app/data/events-default/mail/post_created', $params)
                ->setFrom($from)
                ->setTo($_to)
                ->setSubject($subject);
        }

        \Yii::$app->mailer->sendMultiple($messages);
    }

    public static function createCustomMessage($from, $to, $subject, $body) {
        return \Yii::$app->mailer
            ->compose()
            ->setTextBody($body)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);
    }

    public static function sendMultiple($messages) {
        \Yii::$app->mailer->sendMultiple($messages);
    }
} 