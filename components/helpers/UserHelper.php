<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 12:39
 */

namespace app\components\helpers;


use yii\helpers\ArrayHelper;

class UserHelper {

    const ROLE_ADMIN = 'admin';
    const ROLE_USER  = 'user';

    const PERM_MANAGE_NOTICES = 'permissionManageNotices';
    const PERM_MANAGE_POSTS   = 'permissionManagePosts';
    const PERM_MANAGE_USERS   = 'permissionManageUsers';

    const PERM_MANAGE_NOTICE_LOGS = 'permissionManageNoticesLogs';
    const PERM_VIEW_POST          = 'permissionViewPost';

    public static function getId() {
        return \Yii::$app->user->id;
    }

    public static function isGuest() {
        return \Yii::$app->user->isGuest;
    }

    public static function getUsername() {
        return self::getModel()->username;
    }

    public static function isAdmin() {
        return in_array(self::ROLE_ADMIN, self::getUserRoles());
    }

    /**
     * @return \yii\rbac\Role[]
     */
    public static function getUserRoles() {
        $roles = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());

        //echo '<pre>'; print_r($roles); echo '</pre>';

        return ArrayHelper::map($roles, 'name', 'name');
    }

    public static function getRoles() {
        return [
            self::ROLE_ADMIN,
            self::ROLE_USER,
        ];
    }

    /**
     * @return null|\app\models\Users
     */
    public static function getModel() {
        return \Yii::$app->user->identity;
    }
} 