<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.04.2016
 * Time: 12:07
 */

namespace app\components\helpers;

use Yii;
use yii\helpers\Html;

class MenuHelper {

    public static function getMainMenu() {
        return [
            ['label' => 'Logs', 'url' => ['/notifications/logs'], 'visible' => !UserHelper::isGuest()],
            ['label' => '|', 'visible' => UserHelper::isAdmin()],
            ['label' => 'Notifications', 'url' => ['/notifications/index'], 'visible' => UserHelper::isAdmin()],
            ['label' => 'Posts', 'url' => ['/posts/index'], 'visible' => UserHelper::isAdmin()],
            ['label' => 'Users', 'url' => ['/users/index'], 'visible' => UserHelper::isAdmin()],
            ['label' => '|', 'visible' => UserHelper::isAdmin()],

            UserHelper::isGuest()
                ? ( ['label' => 'Login', 'url' => ['/site/login']] )
                : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . UserHelper::getUsername() . ')',
                        ['class' => 'btn btn-link logout-btn']
                    )
                    . Html::endForm()
                    . '</li>'
                ),

            ['label' => 'Register', 'url' => ['/site/register'], 'visible' => UserHelper::isGuest()],
        ];
    }
} 