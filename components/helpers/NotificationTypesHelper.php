<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.04.2016
 * Time: 19:23
 */

namespace app\components\helpers;


use app\models\Notifications;

class NotificationTypesHelper {

    protected static $types = [
        Notifications::TYPE_EMAIL,
        Notifications::TYPE_BROWSER,
    ];

    public static function getTypes() {
        return self::$types;
    }

    public static function getTypesForList() {
        return array_combine(self::$types, self::$types);
    }

    public static function hasType($type) {
        return in_array($type, self::$types);
    }
} 