<?php

namespace app\components\helpers;


class FlashHelper {

    const FLASH_TYPE_ERROR   = 'error';
    const FLASH_TYPE_WARNING = 'warning';
    const FLASH_TYPE_SUCCESS = 'success';
    const FLASH_TYPE_INFO    = 'information';

    /**
     * @param $type string message type from const
     * @param $message string message body
     */
    public static function setFlash($type, $message) {
        \Yii::$app->getSession()->setFlash($type, $message);
    }

    public static function setFlashInfo($message) {
        self::setFlash(self::FLASH_TYPE_INFO, $message);
    }

    public static function setFlashWarning($message) {
        self::setFlash(self::FLASH_TYPE_WARNING, $message);
    }

    public static function setFlashSuccess($message) {
        self::setFlash(self::FLASH_TYPE_SUCCESS, $message);
    }

    public static function setFlashError($message) {
        self::setFlash(self::FLASH_TYPE_ERROR, $message);
    }
}