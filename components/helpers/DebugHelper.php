<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2016
 * Time: 16:51
 */

namespace app\components\helpers;


class DebugHelper {

    public static function flush($info) {
        \Yii::info(print_r($info, true), 'debug');
    }
} 