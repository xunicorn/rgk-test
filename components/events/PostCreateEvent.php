<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 12:46
 */

namespace app\components\events;


use yii\base\Event;

class PostCreateEvent extends Event {
    public $id;
    public $title;
    public $text_short;
} 