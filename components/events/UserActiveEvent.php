<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 13:43
 */

namespace app\components\events;


use yii\base\Event;

class UserActiveEvent extends Event {
    public $active;
    public $username;
    public $email;

} 