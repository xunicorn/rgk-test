<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2016
 * Time: 12:07
 */

namespace app\components\events;

use yii\base\Event;

class UserRegistrationEvent extends Event {
    public $username;
    public $password;
    public $email;
}