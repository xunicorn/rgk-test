<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            'username',
            //'password',
            [
                'attribute' => 'active',
                'value'     => function($data) {
                    return $data->active;
                },
                'contentOptions'   => ['class' => 'active-state'],
            ],
            [
                'attribute' => 'roles',
                'value'     => function($data) {
                    return implode(',', \app\models\Users::getUserRoles($data->id));
                }
            ],
            [
                'attribute' => 'date_reg',
                'value' => function($data) {
                    return date('Y-m-d H:i:s', $data->date_reg);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>
</div>

<script type="text/javascript">
    $(function() {
        $('.active-state').on('click', changeActiveState);


    });

    function changeActiveState() {

        var $obj = $(this);

        var active = $obj.text();
        var id     = $obj.parents('tr').data('key');

        var new_active = active == '1' ? '0' : '1';

        var data_active = { 'active': new_active };

        var data_route = { 'RouteParams': { 'route': '/users/change-active', 'params': { 'id': id } } };


        $.post(
            '<?= \yii\helpers\Url::to(['/site/create-url']); ?>',
            data_route,
            function(resp) {
                if(resp.success) {
                    sendRequest(resp.url, data_active, $obj);
                }
            },
            'json'
        );
    }

    function sendRequest(url, data, $obj) {

        $.post(
            url,
            data,
            function(resp) {
                var n = Noty('id');

                if(resp.success) {
                    $obj.text(resp.active);

                    $.noty.setText(n.options.id, 'Active state successfully changed');
                    $.noty.setType(n.options.id, 'success');
                } else {
                    $.noty.setText(n.options.id, 'Occurred some error in request');
                    $.noty.setType(n.options.id, 'error');
                }
            },
            'json'
        ).fail(function() {
                var n = Noty('id');

                $.noty.setText(n.options.id, 'Occurred some error in request');
                $.noty.setType(n.options.id, 'error');
            });
    }
</script>
