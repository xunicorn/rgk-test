<?php

use app\components\helpers\ModelEventsHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\helpers\NotificationTypesHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Notifications */
/* @var $form yii\widgets\ActiveForm */

$events = ModelEventsHelper::getEvents();

$short_tags_map = ModelEventsHelper::getInfo();

$shortTags = [];

//foreach($short_tags_map as $_event => ) {}

echo Html::hiddenInput('short-tags', null, array('id' => 'shortTags', 'data-tags' => json_encode(ModelEventsHelper::getInfo())));

$users = \app\models\Users::getUsers();
$users = ArrayHelper::map($users, 'id', 'username');
?>

<div class="notifications-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'event_code')->dropDownList($events); ?>

    <?= $form->field($model, 'user_id_from')->dropDownList($users) ?>

    <?= $form->field($model, 'user_id_to')->dropDownList($users) ?>

    <?= $form->field($model, 'send_to_all')->checkbox(); ?>

    <?= $form->field($model, 'active')->checkbox(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->listBox(NotificationTypesHelper::getTypesForList(), ['multiple' => true]); //array() ?>

    <?= $form->field($model, 'text')->widget(\yii\redactor\widgets\Redactor::className(), [ 'options' => ['rows' => 6]]) ?>

    <div class="hint">
        <small>
            Available tags:
            <span class="available-tags">
                <?php
                    $shortTags = ModelEventsHelper::getInfo($model->isNewRecord ? current($events) : $model->event_code);

                    echo implode(' ', $shortTags);
                ?>
            </span>
        </small>
    </div>

    <hr/>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(function() {
        $('#notifications-event_code').on('change', updateTags);
    });

    function updateTags() {
        var _event = $(this).val();

        var shortTags = $('#shortTags').data('tags');

        if(shortTags.length == 0) {
            return;
        }

        if(!shortTags.hasOwnProperty(_event)) {
            return;
        }

        var tags = shortTags[_event];

        $('.available-tags').html(tags.join(' '));
    }
</script>
