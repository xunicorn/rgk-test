<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Notifications', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'event_code',
            [
                'attribute' => 'user_id_from',
                'value'     => function($data) {
                    return $data->userFrom->username;
                    //echo '<pre>'; print_r($data->userFrom); echo '</pre>';
                }
            ],
            [
                'attribute' => 'user_id_to',
                'value'     => function($data) {
                    return $data->userTo->username;
                    //echo '<pre>'; print_r($data->userTo); echo '</pre>';
                }
            ],
            [
                'attribute' => 'type',
                'value'     => function($data) {
                    return implode(', ', $data->getType());
                }
            ],
            'send_to_all',
            'active',
            'title',
            // 'text:ntext',
            // 'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
