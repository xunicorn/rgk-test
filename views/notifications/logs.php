<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
?>

<h2>Notifications</h2>
<div>
    <?= \yii\helpers\Html::a('Read All', ['close-all'], ['class' => 'btn btn-info']); ?>
</div>

<?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'     => '_log',
]);

?>

<script type="text/javascript">
    $(function() {
        $('.close-btn a').on('click', closeNotification);
    });

    function closeNotification() {
        var $obj = $(this);
        var $container = $(this).parents('.notification-log');

        var id = $container.data('id');

        var data = { 'id': id };

        $.post(
            '<?= \yii\helpers\Url::toRoute(['close-log'])?>',
            data,
            function(resp) {
                if(resp.success) {
                    $obj.hide();

                    $container
                        .removeClass('well')
                        .addClass('alert alert-success');
                } else {
                    var n = Noty('id');

                    $.noty.setText(n.options.id, 'Occurred some error in request');
                    $.noty.setType(n.options.id, 'error');
                }
            },
            'json'
        ).fail(function() {
                var n = Noty('id');

                $.noty.setText(n.options.id, 'Occurred some error in request');
                $.noty.setType(n.options.id, 'error');
            });

        return false;
    }
</script>