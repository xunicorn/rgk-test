<?php
use app\models\NotificationsLogs;


/* @var $model NotificationsLogs */

//echo '<pre>'; print_r($model); echo '</pre>';

$css = ['notification-log'];

if($model->closed) {
    $css[] = 'alert';
    $css[] = 'alert-success';
} else {
    $css[] = 'well';
}
?>

<div class="<?= implode(' ', $css); ?>" data-id="<?= $model->id; ?>">
    <div class="row">
        <div class="title col-xs-11">
            <?= (!empty($model->notification)) ? $model->notification->title : ''; ?>
        </div>
        <?php if(!$model->closed): ?>
        <div class="close-btn col-xs-1">
            <?= \yii\helpers\Html::a('Read', '#'); ?>
        </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="date col-xs-2">
            <?= date('Y-m-d H:i:s', $model->date); ?>
        </div>
        <?php if(!empty($model->notification)): ?>
        <div class="from col-xs-2">
            From: <?= $model->notification->userFrom->username; ?>
        </div>
        <?php endif; ?>
    </div>
    <hr/>
    <div class="text">
        <?= $model->text; ?>
    </div>
</div>