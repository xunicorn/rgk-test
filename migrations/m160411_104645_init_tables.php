<?php

use yii\db\Migration;
use yii\db\Schema;

class m160411_104645_init_tables extends Migration
{
    public function up()
    {
        $this->createTable('notifications', [
            'id'           => Schema::TYPE_PK,
            'event_code'   => Schema::TYPE_STRING . ' NOT NULL',
            'user_id_from' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id_to'   => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'        => Schema::TYPE_STRING . ' NOT NULL',
            'text'         => Schema::TYPE_TEXT,
            'send_to_all'  => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'active'       => Schema::TYPE_SMALLINT . ' DEFAULT 0',
        ]);

        $this->createTable('notifications_logs', [
            'id'              => Schema::TYPE_PK,
            'notification_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date'            => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id'         => Schema::TYPE_INTEGER . ' NOT NULL',
            'text'            => Schema::TYPE_TEXT,
            'type'            => Schema::TYPE_STRING . ' NOT NULL',
            'closed'          => Schema::TYPE_SMALLINT . ' DEFAULT 0',
        ]);

        $this->createTable('notifications_types', [
            'notification_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type'            => Schema::TYPE_STRING . ' NOT NULL',
            'PRIMARY KEY (notification_id, type)'
        ]);

        $this->createTable('posts', [
            'id'         => Schema::TYPE_PK,
            'user_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'      => Schema::TYPE_STRING . ' NOT NULL',
            'text'       => Schema::TYPE_TEXT . ' NOT NULL',
            'text_short' => Schema::TYPE_TEXT . ' NOT NULL',
            'date'       => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createTable('users', [
            'id'           => Schema::TYPE_PK,
            'email'        => Schema::TYPE_STRING . ' NOT NULL',
            'username'     => Schema::TYPE_STRING . ' NOT NULL',
            'password'     => Schema::TYPE_STRING . ' NOT NULL',
            'active'       => Schema::TYPE_SMALLINT . ' DEFAULT 1 NOT NULL',
            'date_reg'     => Schema::TYPE_INTEGER . ' NOT NULL',
            'auth_key'     => Schema::TYPE_STRING . ' NOT NULL',
            'access_token' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        //$this->addPrimaryKey('notifications_types-pk', 'notifications_types', ['notification_id', 'type']);

        $this->createIndex('posts-user', 'posts', ['user_id']);

        $this->createIndex('notifications_logs-user', 'notifications_logs', ['user_id']);
        $this->createIndex('notifications_logs-notification', 'notifications_logs', ['notification_id']);

        $this->createIndex('notifications-user-from', 'notifications', ['user_id_from']);
        $this->createIndex('notifications-user-to', 'notifications', ['user_id_to']);
    }

    public function down()
    {
        echo "m160411_104645_init_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
