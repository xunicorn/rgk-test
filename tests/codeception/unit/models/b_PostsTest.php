<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.04.2016
 * Time: 17:50
 */

namespace app\tests\codeception\unit\models;


use app\components\events\PostCreateEvent;
use app\models\Posts;
use app\models\Users;
use Codeception\Specify;
use yii\base\Event;

class b_PostsTest extends \Codeception\TestCase\Test {

    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

    }

    protected function _after() {
        Posts::deleteAll();
    }

    public function testCreatePost() {
        $user = Users::findByUsername('admin');

        $post_id = Posts::createPost('demo', 'demo Post', 'demo Post Short', $user->id);

        expect('post was created', !empty($post_id))->true();
    }

    public function testCreatePostEvent() {

        $user = Users::findByUsername('admin');

        $GLOBALS['event_fired'] = false;

        $eventFunction = function($event) {
            expect('event from user active state', ($event instanceof PostCreateEvent))->true();

            $GLOBALS['event_fired'] = true;
        };

        Event::on(Posts::className(), Posts::EVENT_POST_CREATE, $eventFunction);

        Posts::createPost('demo', 'demo Post', 'demo Post Short', $user->id);

        expect('POST_CREATE event fired', $GLOBALS['event_fired'])->true();
    }
}
 