<?php
namespace models;


use app\components\events\UserActiveEvent;
use app\components\events\UserRegistrationEvent;
use app\components\helpers\UserHelper;
use app\models\Users;
use Codeception\Specify;
use yii\base\Event;
use yii\helpers\ArrayHelper;

class a_UsersTest extends \Codeception\TestCase\Test
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

    }

    protected function _after()
    {
        $this->clearDb();
    }

    protected function clearDb() {
        $users = Users::find()
            ->where('username <> :admin', [':admin' => 'admin'])
            ->all();

        if(!empty($users)) {
            foreach ($users as $_user) {
                \Yii::$app->authManager->revokeAll($_user->id);
                $_user->delete();
            }
        }
    }

    public function testValidation() {
        $this->specify('test unique usernames and emails', function() {
            $model = new Users();
            $model->username = 'test_u1';
            $model->password = 'test_u1';
            $model->email    = 'test_u1@mail.kom';
            $model->date_reg = time();

            expect('model is valid', $model->validate())->true();

            $model->save();

            $new_model = new Users();
            $new_model->username = 'test_u1';
            $new_model->password = 'test_u1';
            $new_model->email    = 'test_u1@mail.kom';
            $new_model->date_reg = time();

            expect('model is not valid', $new_model->validate())->false();
            expect('not valid username', $new_model->hasErrors('username'))->true();
            expect('not valid email', $new_model->hasErrors('email'))->true();
        });

        $this->specify('test password min length', function() {
            $model = new Users();
            $model->username = 'test_u2';
            $model->password = 'test';
            $model->email    = 'test_u2@mail.kom';
            $model->date_reg = time();

            expect('model is not valid', $model->validate())->false();
            expect('not valid password', $model->hasErrors('password'))->true();
        });
    }

    public function testUserCreation() {
        //$this->specify('user creation', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            $user = Users::findOne(['username' => 'test_u1']);

            expect('user successfully created', !empty($user))->true();
        //});
    }

    public function testUserCreationEvents() {
        //$this->specify('user registration event', function() {
            $GLOBALS['event_fired'] = false;

            $eventFunction = function($event) {
                expect('event from user active state', ($event instanceof UserRegistrationEvent))->true();

                $GLOBALS['event_fired'] = true;
            };

            Event::on(Users::className(), Users::EVENT_USER_REG, $eventFunction);

            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            expect('event fired', $GLOBALS['event_fired'])->true();
        //});
    }

    public function testUserCreationWithRoles() {
        //$this->specify('user creation with roles', function() {
            $roles = [UserHelper::ROLE_ADMIN];

            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom', $roles);

            $user = Users::findOne(['email' => 'test_u1@mail.kom']);

            $roles = \Yii::$app->authManager->getRolesByUser($user->id);
            $roles = ArrayHelper::map($roles, 'name', 'name');

            $success = in_array(UserHelper::ROLE_ADMIN ,$roles);

            expect('user has admin role', $success)->true();
        //});
    }

    public function testValidatePassword() {
        //$this->specify('validate user password', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            $user = Users::findOne(['username' => 'test_u1']);

            expect('password verified', $user->validatePassword('test_u1'))->true();
        //});
    }

    public function testFindByUsername() {
        //$this->specify('find by username', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            $user = Users::findByUsername('test_u1');

            expect('user exists', !empty($user))->true();
        //});
    }

    public function testGetUserRoles() {
        //$this->specify('get users roles', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom', [UserHelper::ROLE_ADMIN, UserHelper::ROLE_USER]);

            $user = Users::findByUsername('test_u1');

            $roles = Users::getUserRoles($user->id);

            //expect('user has role admin', $roles)->contains(UserHelper::ROLE_ADMIN);
            expect('user has role admin', in_array(UserHelper::ROLE_ADMIN, $roles))->true();
            //expect('user has role user', $roles)->contains(UserHelper::ROLE_USER);
            expect('user has role user', in_array(UserHelper::ROLE_USER, $roles))->true();
        //});
    }

    public function testChangeUserRoles() {
        Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom', [UserHelper::ROLE_USER]);

        $user = Users::findByUsername('test_u1');

        $roles = Users::getUserRoles($user->id);

        expect('one role - user', (count($roles) == 1 and in_array(UserHelper::ROLE_USER, $roles)))->true();

        $user->roles = [UserHelper::ROLE_ADMIN];
        $user->save();

        $roles = Users::getUserRoles($user->id);

        expect('one role - admin', (count($roles) == 1 and in_array(UserHelper::ROLE_ADMIN, $roles)))->true();
    }

    public function testGetUsers() {
        //$this->specify('get users', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');
            Users::createUser('test_u2', 'test_u2', 'test_u2@mail.kom');

            $count = Users::find()->count();

            $users = Users::getUsers();

            expect('users count - 3', ($count == count($users) and count($users) == 3))->true();
        //});
    }

    public function testChangeActiveState() {
        //$this->specify('change user active state', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            $user = Users::findByUsername('test_u1');

            expect('user is active', $user->active == 1)->true();

            Users::changeActiveState($user->id, 0);

            $user = Users::findByUsername('test_u1');

            expect('user is disabled', empty($user->active))->true();
        //});
    }

    public function testChangeActiveStateEvent() {
        //$this->specify('change user active state event', function() {
            Users::createUser('test_u1', 'test_u1', 'test_u1@mail.kom');

            $user = Users::findByUsername('test_u1');

            $GLOBALS['event_fired'] = false;

            $eventFunction = function($event) {
                expect('event from user registration', ($event instanceof UserActiveEvent))->true();

                $GLOBALS['event_fired'] = true;
            };

            Event::on(Users::className(), Users::EVENT_USER_ACTIVE, $eventFunction);

            Users::changeActiveState($user->id, 0);

            expect('event fired', $GLOBALS['event_fired'])->true();
        //});
    }
}