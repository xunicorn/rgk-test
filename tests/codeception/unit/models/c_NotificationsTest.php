<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.04.2016
 * Time: 18:32
 */

namespace app\tests\codeception\unit\models;

use app\models\Notifications;
use app\models\Posts;
use app\models\Users;
use Codeception\Specify;

class c_NotificationsTest extends \Codeception\TestCase\Test {

    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after() {
        Notifications::deleteAll();
    }

    /**
     * @return Notifications
     */
    protected function getTestNotification() {
        $user = Users::findByUsername('admin');

        $notif = new Notifications();

        $notif->event_code = Posts::EVENT_POST_CREATE;
        $notif->title      = 'Test notification';
        $notif->type       = [Notifications::TYPE_BROWSER, Notifications::TYPE_EMAIL];

        $notif->user_id_to   = $user->id;
        $notif->user_id_from = $user->id;

        $notif->text = 'Test notification';

        $notif->save();

        return $notif;
    }

    public function testSetNotificationType() {
        $notif = $this->getTestNotification();

        /**
         * @var $notif Notifications
         */
        $notif = Notifications::findOne($notif->id);

        $types = $notif->getType();

        expect('notification has type BROWSER', in_array(Notifications::TYPE_BROWSER, $types))->true();
        expect('notification has type EMAIL', in_array(Notifications::TYPE_EMAIL, $types))->true();

        $notif->type = Notifications::TYPE_BROWSER;
        $notif->save();

        $notif = Notifications::findOne($notif->id);

        $types = $notif->getType();

        expect('notification has type BROWSER', in_array(Notifications::TYPE_BROWSER, $types))->true();
        expect('notification has not type EMAIL', !in_array(Notifications::TYPE_EMAIL, $types))->true();
    }

    public function testIsEmail() {
        $notif = $this->getTestNotification();

        expect('notification has type EMAIL', $notif->isEmail())->true();
    }

    public function testIsBrowser() {
        $notif = $this->getTestNotification();
        $notif->type = Notifications::TYPE_EMAIL;
        $notif->save();

        expect('notification has not type BROWSER', !$notif->isBrowser())->true();
    }

    public function testGetNotificationsByEvent() {
        $count = 5;

        for($i = 0; $i < $count; $i++) {
            $this->getTestNotification();
        }

        $notifs = Notifications::getNotificationsByEvent(Posts::EVENT_POST_CREATE);

        expect('there are 5 notifications', $notifs)->count($count);
    }

    public function testGetUserNotifications() {
        $count = 5;

        for($i = 0; $i < $count; $i++) {
            $_notif = $this->getTestNotification();

            $_notif->type = ($i < 3) ? Notifications::TYPE_BROWSER : Notifications::TYPE_EMAIL;

            $_notif->save();
        }

        $user = Users::findByUsername('admin');

        $notifs_browser = Notifications::getUserNotifications($user->id, Notifications::TYPE_BROWSER);
        $notifs_email   = Notifications::getUserNotifications($user->id, Notifications::TYPE_EMAIL);

        expect('there are 3 BROWSER notifications', $notifs_browser)->count(3);
        expect('there are 2 EMAIL notifications', $notifs_email)->count(2);
    }
}
 