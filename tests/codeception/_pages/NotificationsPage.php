<?php

use yii\codeception\BasePage;

/**
 * Class NotificationsPage
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class NotificationsPage  extends BasePage
{
    public $route = 'notifications/index';

    public function fillCreateForm($title, array $types, $text) {
        $this->actor->fillField('#notifications-title', $title);
        $this->actor->selectOption('#notifications-type', $types);
        $this->actor->fillField('#notifications-text', $text);
    }
}