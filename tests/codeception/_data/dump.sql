-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table yii2.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table yii2.auth_assignment: ~2 rows (approximately)
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('admin', '1', 1460386332);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;


-- Dumping structure for table yii2.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table yii2.auth_item: ~7 rows (approximately)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('admin', 1, 'admin role', NULL, NULL, 1460385740, 1460385740),
	('permissionManageNotices', 2, 'CRUD for Notifications', NULL, NULL, 1460385740, 1460385740),
	('permissionManageNoticesLogs', 2, 'View/Close notifications logs', NULL, NULL, 1460385740, 1460385740),
	('permissionManagePosts', 2, 'CRUD for Posts', NULL, NULL, 1460385740, 1460385740),
	('permissionManageUsers', 2, 'CRUD for Users', NULL, NULL, 1460385740, 1460385740),
	('permissionViewPost', 2, 'View Post', NULL, NULL, 1460385740, 1460385740),
	('user', 1, 'auth user role', NULL, NULL, 1460385740, 1460385740);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;


-- Dumping structure for table yii2.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table yii2.auth_item_child: ~6 rows (approximately)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('admin', 'permissionManageNotices'),
	('user', 'permissionManageNoticesLogs'),
	('admin', 'permissionManagePosts'),
	('admin', 'permissionManageUsers'),
	('user', 'permissionViewPost'),
	('admin', 'user');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;


-- Dumping structure for table yii2.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table yii2.auth_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;


-- Dumping structure for table yii2.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.migration: ~2 rows (approximately)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1460385711),
	('m140506_102106_rbac_init', 1460385736),
	('m160411_104645_init_tables', 1460385717);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;


-- Dumping structure for table yii2.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_code` varchar(255) NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `user_id_to` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text,
  `send_to_all` smallint(6) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `notifications-user-from` (`user_id_from`),
  KEY `notifications-user-to` (`user_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;


-- Dumping structure for table yii2.notifications_logs
CREATE TABLE IF NOT EXISTS `notifications_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text,
  `type` varchar(255) NOT NULL,
  `closed` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `notifications_logs-user` (`user_id`),
  KEY `notifications_logs-notification` (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.notifications_logs: ~2 rows (approximately)
/*!40000 ALTER TABLE `notifications_logs` DISABLE KEYS */;
INSERT INTO `notifications_logs` (`id`, `notification_id`, `date`, `user_id`, `text`, `type`, `closed`) VALUES
	(1, 0, 1460389306, 1, 'Dear admin. On our <a href="http://yii2">Test Application</a> was posted new article.<br/>\r\n\r\n<h4>1st post</h4>\r\n<p><p>1st short</p></p>\r\n<p><a href="http://yii2/index.php?r=posts%2Fview&amp;id=1">Read more</a></p>\r\n<br/>\r\nBest regards <a href="http://yii2">Test Application</a> Team.', 'browser', 0),
	(2, 0, 1460389306, 2, 'Dear test_u1. On our <a href="http://yii2">Test Application</a> was posted new article.<br/>\r\n\r\n<h4>1st post</h4>\r\n<p><p>1st short</p></p>\r\n<p><a href="http://yii2/index.php?r=posts%2Fview&amp;id=1">Read more</a></p>\r\n<br/>\r\nBest regards <a href="http://yii2">Test Application</a> Team.', 'browser', 0);
/*!40000 ALTER TABLE `notifications_logs` ENABLE KEYS */;


-- Dumping structure for table yii2.notifications_types
CREATE TABLE IF NOT EXISTS `notifications_types` (
  `notification_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`notification_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.notifications_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `notifications_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications_types` ENABLE KEYS */;


-- Dumping structure for table yii2.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `text_short` text NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `posts-user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `user_id`, `title`, `text`, `text_short`, `date`) VALUES
	(1, 1, '1st post', '<p>1st long</p>', '<p>1st short</p>', 1460389305);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;


-- Dumping structure for table yii2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `date_reg` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table yii2.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `username`, `password`, `active`, `date_reg`, `auth_key`, `access_token`) VALUES
	(1, 'admin@mail.kom', 'admin', '$2y$13$6CeK1ZSd3j9ZkKmWlVGAIeyuF6X3JkhH7rFCG5vKtebEISfedrxyy', 1, 1460386065, '0O-wdh55yXoJWunpvPiT3oXRfrSttDPv', '1DR4eWbDJpa5XN6ZrDxUfrnYEzUmHWTe');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
