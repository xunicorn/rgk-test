<?php

/* @var $scenario Codeception\Scenario */

use tests\codeception\_pages\LoginPage;

$I = new FunctionalTester($scenario);


$loginPage = LoginPage::openBy($I);

$I->see('Login', 'h1');

$I->amGoingTo('try to login with correct credentials');
$loginPage->login('admin', 'admin');
$I->expectTo('see user info');
$I->see('Logout (admin)');

$I->canSeeLink('Logs');
$I->see('Read All');

