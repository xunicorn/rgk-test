<?php

/* @var $scenario Codeception\Scenario */

use tests\codeception\_pages\LoginPage;

$I = new FunctionalTester($scenario);

$loginPage = LoginPage::openBy($I);

$I->see('Login', 'h1');

$I->amGoingTo('try to login with correct credentials');
$loginPage->login('admin', 'admin');
$I->expectTo('see user info');
$I->see('Logout (admin)');

$notifPage = NotificationsPage::openBy($I);
$I->expectTo('notifications admin page');
$I->see('Create Notifications');

$I->amGoingTo('go to notifications create page');
$I->click('Create Notifications');
$I->expectTo('notifications create page');
$I->see('Available tags');

$I->amGoingTo('go to create empty notification');
NewNotificationPage::openBy($I);
//$I->click('Create');
$I->expectTo('validation errors');
$I->see('Title cannot be blank.');
$I->see('Type cannot be blank.');
$I->see('Text cannot be blank.');
//i->see('Hello Wold!');

$notificationTitle = 'Test notification qwerty';

/* @ToDo: not finished test */
$I->amGoingTo('create test notification');
NewNotificationPage::openBy($I);
$notifPage->fillCreateForm($notificationTitle, ['email', 'browser'], 'Test text');
$I->click('.notifications-form .btn[type=submit]');

$I->expectTo('see notifs admin page & recently create test notif');
$I->see($notificationTitle);