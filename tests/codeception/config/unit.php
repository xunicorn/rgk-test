<?php
/**
 * Application configuration for unit tests
 */
$configs = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../../config/web.php'),
    require(__DIR__ . '/config.php'),
    [
    ]
);

if(isset($configs['components']['eventsExt'])) {
    unset($configs['components']['eventsExt']);
    $key = array_search('eventsExt', $configs['bootstrap']);

    if($key !== false) {
        unset($configs['bootstrap'][$key]);
    }
}

return $configs;
