Yii 2 Test Project
============================

This is a test project for one company on [Yii 2](http://www.yiiframework.com/), basic template. For the framework installation see [here](https://github.com/yiisoft/yii2) 

Installation
============================

1. Install basic application - [link](http://www.yiiframework.com/doc-2.0/guide-start-installation.html)
2. use migrate m160411_104645_init_tables from repo
3. use migrate for rbac: yii migrate --migrationPath=@yii/rbac/migrations/
4. init rbac: yii rbac/init

About
============================

This project allows user registration with RBAC. By default system has 2 roles: user & admin. First user will have admins role, next - users.
This simple system allows create, notifies users on post creation. 

Also you could write your own notifications on different events. For this you should write yours handlers in app\components\EventsExtComponent :: init().
By default system has three notifications on different events. They are sending via email & they're placed in the end of each event callback function.
Content for default emails are getting from \app\data\events-default\mail\* files. See some of them for some examples. They are using from \app\components\helpers\MailerHelper class.
You are also can disable default event notification by placing config parameter {useDefaultNotice} to false. Than system will be use notifications created from section Notifications.

For each user-defined notification you should define notification text body (with short tags), sender, recipients. You could create your own short tags for event, but you have to describe its in app\component\helpers\ModelEventsHelper::$eventsRoadMap.
Also you should describe new tags parsing process in \app\components\EventsExtComponent::$parseTags
  
  
Be aware! Default events triggering only in such methods: 

\app\models\Users::createUser()        - \app\models\Users::EVENT_USER_REG

\app\models\Users::changeActiveState() - \app\models\Users::EVENT_USER_ACTIVE

\app\models\Posts::createPost()        - \app\models\Posts::EVENT_POST_CREATE

If you added new notification type - you have to add this type to \app\components\helpers\NotificationTypesHelper::$types

Development time - 12 hrs